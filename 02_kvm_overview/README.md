# KVM Deep Dive

.footer: Created By Alex M. Schapelle, VaioLabs.IO

---
# KVM Overview

What is KVM ?  Kernel based Virtual Machine

- KVM is hypervisor which is built in to Linux Kernel
- AllowsLinux desktops  or servers to simulate multiple pieces of hardware
- Full virtualization solution for x86/amd64 hardware architecture that contains virtualixation extensions such as Intel-VT or AMD-V
- KVM uses the QEMU (Quich Emulator) virtual machine format
- KVM is managed with LibVirt Daemon via Libvirt API with its own shell called `virsh`.
  -  its main goal is to `speed up` communication between QEMU and Linux Kernel
  -  Virtual machines are executed and managed mainly with `QEMU` and `Libvirt` as an multiple threaded processes.

---
