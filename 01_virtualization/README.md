# KVM Deep Dive

.footer: Created By Alex M. Schapelle, VaioLabs.IO

---
# What is virtualization ?
In computing, virtualization or virtualisation (sometimes abbreviated v12n, a numeronym) is the act of creating a virtual (rather than actual) version of something, including virtual computer hardware platforms, storage devices, and computer network resources.

Virtualization began in the 1960s, as a method of logically dividing the system resources provided by mainframe computers between different applications. Since then, the meaning of the term has broadened.

---
# Hardware Virtualization

Hardware virtualization or platform virtualization refers to the creation of a virtual machine that acts like a real computer with an operating system. Software executed on these virtual machines is separated from the underlying hardware resources


---
# Hardware Virtualization (cont.)


- Full Virtualization
	- Complete simulation of the actual hardware to allow softwate, which typically consists of a guest machine or virtual machine
	- KVM uses full virtualization

- Para-Virtualization
	- the hardware environment is not simulated


---
# Desktop Virtualization

- Desktop virtualization is concept of seperating the logical desktop from physical machine
- VDI: Virtual Dekstop Infrastructure

---
# Hypervisors

- VMM/Hypervisor is a piese of software that is responsible for monitoring and contrilling virtual machines or guest operating systems

- Type 1 Hypervisor
	- The hypervisor run directly on top of the hardware

- Type 2 Hypervisor
	- The hypervisor acts as a seperate layer often on top of base Operating System

---
# Hypervisors (cont.)
Type 1


---
# Hypervisors (cont.)
Type 2

---
# Overcommiting

- To allocate more virtualized CPUs or virtual memory than the available resource on the host system provides
- Overcommiting can cause possible risk to your host systems stability

---
# Thin Provisioning

- Allows you to optimize available storage space for the guest virtual machines
- Similar to overcommiting, but only pertains to storage, not CPU and memory
- Can also pose risk to the system stability

---
# Nested Virtualization
While using virtualization in specific environments is needed, it may be extended with
`Nested virtualization`.
- Nested Virtualization refers to the ability of running a virtual machine within another, having this general concept extendable to an arbitrary depth. 
  - In other words, nested virtualization means, running one or more vm inside other vm.
  - Nature of a nested guest virtual machine does not need not be homogeneous with its host virtual machine; for example, application virtualization can be deployed within a virtual machine created by using hardware virtualization.

---
# Linux Virtualization

Type 2 hypervisor, also known as Host/Guest Virtualization

- Process through which one or more virtual machines can be installed, eexecuted and maintained on top of the Linux OS
-  Linux virtualization brings opennes, flexability and high performance.

---
# Open Source Virtualization projets

- KVM: Kernel-based Virtual Machine
- Xen
- Virtualbox
- UML: User Mode Linux

---
# Kernel Same-Page Merging(KSM)

- Allows KVM guests to share identical memory pages
- Shared common libraries or other identical, high-use data
- **KSM essentially allows to use larger number of guests on the hosts, because it avoids memory duplication on the HOST**
 
---
# The Heavy Lifter: QEMU

Qemu guest agent is the main software that runs on the guest virtual machines openrating system in order to issue commands to the guest OS from the host OS.

---
# Linux Virtualizatino and the cloud

- Public:
  - AWS
  - GCP
  - AZURE
  - Digital Ocean
  
- Private:
  - OpenStack
  - CloudStack
  - Eucalytus

---
# Epilog

We went through understanding what Virtualization is and how and where it is implemented